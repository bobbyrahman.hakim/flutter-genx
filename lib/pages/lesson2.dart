import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LessonTwo extends StatelessWidget {
  const LessonTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        home: Scaffold(
      appBar: AppBar(),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () => Get.back(),
                  child: const Text(
                    "Back Home",
                    style: TextStyle(fontSize: 20),
                  )),
            ],
          )
        ],
      ),
    ));
  }
}
