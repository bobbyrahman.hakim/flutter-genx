import 'package:flutter/material.dart';
import 'package:genx/controllers/counter_controller.dart';
import 'package:get/get.dart';

class LessonSatu extends StatelessWidget {
  LessonSatu({Key? key}) : super(key: key);
  final counterC = Get.put(CounterController());
  final cC = Get.find<CounterController>();
  @override
  Widget build(BuildContext context) {
    return Obx(() => GetMaterialApp(
          theme: counterC.isDark.value ? ThemeData.dark() : ThemeData.light(),
          home: Scaffold(
              appBar: AppBar(),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          onPressed: () => cC.increment(),
                          child: const Text(
                            "+",
                            style: TextStyle(fontSize: 20),
                          )),
                      Center(
                        child: Obx(
                          () => Text(
                            "Angka ${cC.counter}",
                            style: const TextStyle(
                              fontSize: 35,
                            ),
                          ),
                        ),
                      ),
                      ElevatedButton(
                          onPressed: () => cC.decrement(),
                          child: const Text(
                            "-",
                            style: TextStyle(fontSize: 20),
                          ))
                    ],
                  ),
                  ElevatedButton(
                      onPressed: () => cC.changeTheme(),
                      child: const Text(
                        "Change Theme",
                        style: TextStyle(fontSize: 20),
                      )),
                  ElevatedButton(
                      onPressed: () => Get.back(),
                      child: const Text(
                        "Back Home",
                        style: TextStyle(fontSize: 20),
                      ))
                ],
              )),
        ));
  }
}
